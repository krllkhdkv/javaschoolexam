package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {

        // Fist of all check if we have null reference or empty list.
        if (x == null || y == null)
            throw new IllegalArgumentException();
        if (x.size() == 0)
            return true;

        // We need to iterate over the second list.
        // Each time we meet equality between lists, we would also shift the first iterator.
        int xIterator = 0;
        for (Object item: y) {
            if (xIterator < x.size() && item.equals(x.get(xIterator)))
                xIterator++;
        }

        // If the first iterator is out of array,
        // it means that we find all items of the first list in the second list.
        return xIterator == x.size();
    }
}
