package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.*;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        Converter converter = new Converter();
        List<String> postfixNotation;

        // Convert string record of statement to postfix notation. If the method below did not throw an exception,
        // we can be sure, that there are no errors in input data.
        try {
            postfixNotation = converter.getPostfixNotation(statement);
        } catch (IllegalArgumentException e) {
            return null;
        }

        // Push numbers from list to stack until we meet an operator. When we meet it, we pop the last two items
        // from the stack, apply current binary operation to them, and push the result back.
        Stack<String> stack = new Stack<>();
        for (String term: postfixNotation) {
            if (converter.isOperation(term)) {
                String b = stack.pop();
                String a = stack.pop();

                try {
                    stack.push(singleOperation(a, b, term));
                } catch (ArithmeticException e) {
                    return null;
                }

            } else {
                stack.push(term);
            }
        }

        // We should have only one number in the stack after all operations. There is an answer to the expression.
        return round(stack.pop());
    }

    /**
     * Performs a single binary operation for numbers stored in strings.
     * @param a the first operand.
     * @param b the second Operand.
     * @param operator a binary operator ("+", "-", "*", "/").
     * @return the result of arithmetical operation.
     * @throws ArithmeticException if division by zero occurred.
     */
    private String singleOperation(String a, String b, String operator) throws ArithmeticException {
        String result;

        // If both numbers are integer, we would try to get integer result of their applying.
        // Otherwise we will get floating-point result.
        try {
            // Firstly check if the string representation of number can be converted to int.
            int aInt = Integer.parseInt(a);
            int bInt = Integer.parseInt(b);
            result = singleOperation(aInt, bInt, operator);

        } catch (NumberFormatException e1) {
            // We can be sure that next two parsings will complete successfully, because we have already tested that
            // there are no error in method Converter.check.
            double aDouble = Double.parseDouble(a);
            double bDouble = Double.parseDouble(b);
            result = singleOperation(aDouble, bDouble, operator);
        }
        return result;
    }

    /**
     * Performs a single binary operation for integer numbers.
     * @param a the first operand.
     * @param b the second operand.
     * @param operator a binary operator ("+", "-", "*", "/").
     * @return the result of arithmetical operation.
     * @throws ArithmeticException if division by zero occurred.
     */
    private String singleOperation(int a, int b, String operator) throws ArithmeticException {
        String result = "";
        switch (operator) {
            case "+":
                result = Integer.toString(a + b);
                break;
            case "-":
                result = Integer.toString(a - b);
                break;
            case "*":
                result = Integer.toString(a * b);
                break;
            case "/":
                if (b == 0)
                    throw new ArithmeticException();

                // If there is no remainder of division return result integral result.
                // Otherwise we have to return floating-point number.
                if (a % b == 0)
                    result = Integer.toString(a / b);
                else
                    result = Double.toString(Integer.valueOf(a).doubleValue() / Integer.valueOf(b).doubleValue());
                break;
        }
        return result;
    }

    /**
     * Performs a single binary operation for double numbers.
     * @param a the first operand.
     * @param b the second operand.
     * @param operator a binary operator ("+", "-", "*", "/").
     * @return the result of arithmetical operation.
     * @throws ArithmeticException If division by zero occurred.
     */
    private String singleOperation(double a, double b, String operator) throws ArithmeticException {
        String result = "";
        switch (operator) {
            case "+":
                result = Double.toString(a + b);
                break;
            case "-":
                result = Double.toString(a - b);
                break;
            case "*":
                result = Double.toString(a * b);
                break;
            case "/":
                if (b == 0)
                    throw new ArithmeticException();
                result = Double.toString(a / b);
                break;
        }
        return result;
    }

    /**
     * Rounds a number to 4 decimal points.
     * @param number a number to round.
     * @return string representation of the rounded value.
     */
    private String round(String number) {
        // Check if the number has point. If the number is integral, return as is. In the opposite case,
        // apply to number an appropriate format.
        if (number.indexOf('.') == -1) {
            return number;

        } else {
            double value = Double.parseDouble(number);
            DecimalFormat formatter = (DecimalFormat) NumberFormat.getNumberInstance(Locale.ROOT);
            formatter.applyPattern("#.####");
            return formatter.format(value);
        }
    }
}
