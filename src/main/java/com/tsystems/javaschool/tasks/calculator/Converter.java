package com.tsystems.javaschool.tasks.calculator;

import java.util.*;

/**
 * The class encapsulates methods which allow to convert arithmetic expressions in suitable forms.
 */
class Converter {

    /**
     * Converts string statement to Reverse Polish notation (postfix notation).
     * @param statement string representation of arithmetic expression.
     * @return ordered ist of strings where each item is either an operand or an operator.
     * @throws IllegalArgumentException if any parts of initial statement cannot be parsed.
     */
    List<String> getPostfixNotation(String statement) throws IllegalArgumentException {
        if (statement == null || statement.length() == 0)
            throw new IllegalArgumentException();

        // We will extract discrete terms (numbers, operators, parentheses) from the given arithmetic expression
        // and put them in list with keeping their default order.
        List<String> infixNotation = new ArrayList<>();

        // We will iterate over the initial string from one char to another. Operators have length = 1,
        // and it's easy to extract them. But for numbers we should have additional index
        // that will point at the beginning of a number.
        int lastOperandIdx = -1;
        char curChar, prevChar;

        for (int i = 0; i < statement.length(); i++)
        {
            prevChar = i > 0 ? statement.charAt(i - 1) : 0;
            curChar = statement.charAt(i);

            // We will explicitly check only presence of operators. If statement has no errors,
            // everything located between operators is numbers.
            if (isParenthesis(curChar) || isBinaryOperation(curChar, prevChar)) {
                if (i - lastOperandIdx > 1) {
                    infixNotation.add(statement.substring(lastOperandIdx + 1, i));
                }
                infixNotation.add(Character.toString(curChar));
                lastOperandIdx = i;
            }
        }
        // We add numbers to list only together with operators. If the last term of expression was number,
        // we have to append it.
        if (statement.length() - lastOperandIdx > 1)
            infixNotation.add(statement.substring(lastOperandIdx + 1));

        return getPostfixNotation(infixNotation);
    }

    /**
     * Converts infix notation to Reverse Polish notation (postfix notation).
     * @param infixNotation list of strings where each item is either an operand or an operator.
     * @return ordered ist of strings where each item is either an operand or an operator.
     * @throws IllegalArgumentException if any items of postfix notation cannot be parsed.
     */
    private List<String> getPostfixNotation(List<String> infixNotation) throws IllegalArgumentException {
        if (!isCorrect(infixNotation))
            throw new IllegalArgumentException();

        List<String> postfixNotation = new ArrayList<>();
        Stack<String> stack = new Stack<>();

        // The map for storing operations priority.
        Map<String, Integer> priorities = new HashMap<>();
        priorities.put("(", 0);
        priorities.put(")", 1);
        priorities.put("+", 2);
        priorities.put("-", 2);
        priorities.put("*", 3);
        priorities.put("/", 3);

        // Iterate over terms in default(infix) order. Each term (except for closing parenthesis) we should push
        // in stack and then add to new list.
        for (String term: infixNotation) {

            if (isOperation(term)) {
                // Operators are added to stack only if peek of stack does not contains other operator
                // that has equal or greater priority. If not, transfer items from stack to output list, until the
                // previous condition become true.
                while (!stack.empty() && priorities.get(stack.peek()) >= priorities.get(term))
                    postfixNotation.add(stack.pop());
                stack.push(term);

            } else if (term.equals("(")) {
                stack.push(term);


            } else if (term.equals(")")) {
                // Closing parenthesis are never pushed in the stack. Instead of this items from stack
                // transfer to output list, until we meet opening parenthesis. After that they both disappear.
                while (!stack.peek().equals("("))
                    postfixNotation.add(stack.pop());
                stack.pop();

            } else {
                // Numbers are sent directly to output list, bypassing the stack.
                postfixNotation.add(term);
            }
        }

        while (!stack.empty()) {
            postfixNotation.add(stack.pop());
        }

        return postfixNotation;
    }

    /**
     * Tests if the statement is correct.
     * @param expressions arithmetic expression written in infix notation.
     * @return true if there are no errors in the statement, false otherwise.
     */
    private boolean isCorrect(List<String> expressions) {
        String expr = expressions.get(0);
        String prevExpr = expressions.get(0);

        // Arithmetic expression can not begin with a closing parenthesis or a binary operator.
        if (expr.equals(")") || isOperation(expr))
            return false;

        // We will increase this counter when we meet opening parentheses and decrease otherwise.
        // There is a mistake in expression, if counter ever was less than zero.
        int parenthesesCounter = 0;
        if (expr.equals("("))
            parenthesesCounter++;

        // The Correctness of the placement of adjacent items is checked in the loop.
        for (int i = 1; i < expressions.size(); i++) {
            expr = expressions.get(i);

            if (expr.equals("(")) {
                parenthesesCounter++;
                if (!isOperation(prevExpr))
                    return false;

            } else if (expr.equals(")")) {
                parenthesesCounter--;
                if (parenthesesCounter < 0 || !isNumber(prevExpr))
                    return false;

            } else if (isOperation(expr)) {
                if (prevExpr.equals("(") || isOperation(prevExpr))
                    return false;

            } else if (isNumber((expr)))  {
                if (prevExpr.equals(")"))
                    return false;
            } else {
                return false;
            }
            prevExpr = expr;
        }

        // An expression can be end with only a number or a closing parenthesis.
        // The parentheses counter must have zero value.
        return (expr.equals(")") || isNumber(expr)) && parenthesesCounter == 0;
    }

    /**
     * Tests if the string is a number (integer or floating point).
     * @param term the string.
     * @return true if the string is a number, false otherwise.
     */
    private boolean isNumber(String term) {
        return term.matches("^[+-]?[0-9]+(\\.[0-9]+)?$");
    }

    /**
     * Tests if the given character is a parentheses (opening or closing).
     * @param ch the character.
     * @return true if the character is parentheses, otherwise false.
     */
    private boolean isParenthesis(char ch) {
        return ch == '(' || ch == ')';
    }

    /**
     * Tests if the given character is a binary operator.
     * @param ch character to check.
     * @param prevChar previous character.
     * @return true if the character is a binary operator, false otherwise.
     */
    private boolean isBinaryOperation(char ch, char prevChar) {
        return isOperation(ch) && !(prevChar == '(' || prevChar == 0);
    }

    /**
     * Tests if the given char is an arithmetic operator.
     * @param ch the character to check.
     * @return true if the character is an operator, false otherwise.
     */
    private boolean isOperation(char ch) {
        return ch == '+' || ch == '-' || ch == '*' || ch == '/';
    }

    /**
     * Tests if the given string is an arithmetic operator.
     * @param str the string to check.
     * @return true if the string is an operator, false otherwise.
     */
    boolean isOperation(String str) {
        return str.length() == 1 && isOperation(str.charAt(0));
    }
}
