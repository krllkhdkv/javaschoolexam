package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {

        // To build pyramid we need array with at least 1 item. Check this condition.
        if (inputNumbers == null || inputNumbers.size() == 0)
            throw new CannotBuildPyramidException();

        // I found out that the dependence of width on height can be expressed by the formula w = h * 2 - 1.
        int height = getHeight(inputNumbers.size()), width = height * 2 - 1;

        // All array items is equal to 0 by default.
        int[][] pyramid = new int[height][width];

        // Sorting of the collection may cause an exception, because we use non-generic version of List.
        try {
            Collections.sort(inputNumbers);
        } catch (Exception e) {
            System.out.println(e.toString());
            throw new CannotBuildPyramidException();
        }

        // We will fill pyramid from top to down and from left to right.
        // Let offset (left offset) be the number of zeroes in a row before the first item from the list.
        // The offset is equal to half width in the first row. The offset decrements each row.
        // We will record sorted list numbers in pyramid after offset and they should be separated by zeroes.
        int i = 0, offset = width / 2, j = offset;
        for (int number: inputNumbers) {
            // Before each attempt to record a number we should check whether right offset is greater
            // than left offset and begin a new row if necessary.
            if (j > width - 1 - offset) {
                i++;
                offset--;
                j = offset;
            }
            pyramid[i][j] = number;
            j += 2;
        }

        return pyramid;
    }

    /**
     * Calculates what height the pyramid will have with the given array.
     * @param size quantity of items in the array.
     * @return the height of the pyramid.
     * @throws CannotBuildPyramidException if it is impossible to create a pyramid with given list size.
     */
    private int getHeight(int size) throws CannotBuildPyramidException {
        // Only a small fraction of arrays can fit the pyramid.
        // I found out the pattern: size = height * (height + 1) / 2;
        // We can configure a loop that calculate all possible array sizes which is equal or less than the given size.
        // The given array would be appropriate for building a pyramid if its size is equal to last calculated size;
        // And we would also know the height of the pyramid.
        int correctSize = 0, height = 0;
        while (correctSize < size && correctSize >= 0) {
            correctSize += height;
            height++;
        }
        height--;

        if (size != correctSize)
            throw new CannotBuildPyramidException();

        return height;
    }
}
